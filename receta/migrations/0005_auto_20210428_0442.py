# Generated by Django 3.2 on 2021-04-28 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('receta', '0004_receta_creada_por'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='receta',
            name='ingrediente',
        ),
        migrations.AddField(
            model_name='ingrediente',
            name='id_receta',
            field=models.IntegerField(default=0),
        ),
    ]
