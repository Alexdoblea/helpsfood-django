# Generated by Django 3.2 on 2021-04-27 01:30

from django.db import migrations, models
import receta.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingrediente',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=30)),
                ('cantidad', models.IntegerField()),
                ('medida', models.CharField(max_length=5)),
            ],
        ),
        migrations.CreateModel(
            name='Receta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=40)),
                ('imagen', models.ImageField(default=None, upload_to=receta.models.rename)),
                ('tipo', models.IntegerField(choices=[(1, 'Diamond'), (2, 'Spade'), (3, 'Heart'), (4, 'Club')])),
                ('categoria', models.IntegerField(choices=[(1, 'Diamond'), (2, 'Spade'), (3, 'Heart'), (4, 'Club')])),
                ('preparacion', models.TextField()),
                ('estado', models.BooleanField(default=True)),
                ('ingrediente', models.ManyToManyField(default=None, to='receta.Ingrediente')),
            ],
        ),
    ]
