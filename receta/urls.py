from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from .views import *

app_name='receta'

urlpatterns = [
    path('ver_recetas', ver_recetas, name='ver_recetas'),
    path('crear_receta', crear_receta, name='crear_receta'),
    path('agregar_ingrediente', agregar_ingrediente, name='agregar_ingrediente'),
    path('quitar_ingrediente', quitar_ingrediente, name='quitar_ingrediente'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)