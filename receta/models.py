from django.db import models
from django.utils import timezone
import os
from uuid import uuid4



#FUNCION QUE SIRVE PARA CAMBIAR EL NOMBRE DE LA IMAGEN SUBIDA Y EL PATH ADEMÁS DE GUARDARLA EN UPLOADS
def rename(instance, filename):
    upload_to = 'uploads'
    ext = filename.split('.')[-1]
    # get filename
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)

class Receta(models.Model):
    titulo=models.CharField(max_length=40, null=True)
    imagen=models.ImageField(upload_to=rename, height_field=None, width_field=None, max_length=None, null=True)

    tipo_choices=[
            ('OMNIVORA','OMNIVORA'),
            ('VEGANA','VEGANA'),
            ('VEGETARIANA','VEGETARIANA')
            ]

    categoria_choices=[
        ('DESAYUNO', 'DESAYUNO'),
        ('ALMUERZO', 'ALMUERZO'),
        ('POSTRE', 'POSTRE'),
        ('MERIENDA', 'MERIENDA'),
        ('ONCE', 'ONCE'),
        ('CENA', 'CENA')
        ]

    tipo = models.CharField(choices=tipo_choices, max_length=50, null=True)
    categoria = models.CharField(choices=categoria_choices, max_length=50, null=True)
    preparacion=models.TextField(null=True)
    creada_por=models.CharField(max_length=50, null=True)
    estado=models.BooleanField(default=False)

    def __str__(self):
        if self.titulo is None:
            return str(self.id)+ ' (En creacion)'
        else:
            return str(self.id)+' Titulo:'+self.titulo

class Ingrediente(models.Model):
    id_receta=models.IntegerField(default=0)
    nombre=models.CharField(max_length=30)
    cantidad=models.IntegerField()
    medida=models.CharField(max_length=10)
    def __str__(self):
        return str(self.cantidad)+' '+self.medida+' '+self.nombre