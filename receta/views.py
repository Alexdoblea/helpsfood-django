from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from receta.forms import *
from receta.models import *
from django.core.files.storage import FileSystemStorage
import os
from uuid import uuid4

# MUESTRA LAS ULTIMAS 4 RECETAS INGRESADAS CUYO ESTADO (APROBACION) SEA VERDADERO(TRUE)
def ver_recetas(request):
    recetas = Receta.objects.all().filter(estado=True).order_by('-id')[:3]
    ingredientes = Ingrediente.objects.all()
    contexto={'recetas':recetas,
            'ingredientes':ingredientes}
    return render(request, 'recetas/ver_recetas.html', contexto)

# PARA CREAR UNA NUEVA RECETA
@login_required(login_url='cliente:iniciar_sesion')
def crear_receta(request):
    Receta.objects.filter(estado=False).filter(creada_por=request.user.username).delete()
    receta=Receta()
    receta.creada_por=request.user.username
    receta.save()
    ultima_receta = Receta.objects.latest('id').id
    if request.method == 'POST':
        r=Receta.objects.filter(id=ultima_receta).first()
        r.estado=True
        r.titulo=request.POST.get('titulo')
        r.tipo=request.POST.get('tipo')
        r.categoria=request.POST.get('categoria')
        r.preparacion=request.POST.get('preparacion')

        #GUARDANDO IMAGEN Y URL DE LA IMAGEN
        mi_imagen = request.FILES['imagen']
        nombre_anterior = mi_imagen.name
        #Renombra la imagen
        def rename(filename):
            ext = filename.split('.')[-1]
            filename = '{}.{}'.format(uuid4().hex, ext)
            return filename
        nombre_nuevo = rename(nombre_anterior)
        fs = FileSystemStorage('media/uploads/')
        filename = fs.save(nombre_nuevo, mi_imagen)
        url_imagen = 'uploads'+fs.url(filename)
        r.imagen=url_imagen
        r.save()
        return redirect('receta:ver_recetas')
    else:
        form = RecetaForm()
        contexto={'form':form, 'ultima_receta': ultima_receta}
        return render(request, 'recetas/crear_receta.html', contexto)

#AGREGA VIA AJAX UN INGREDIENTE Y LO MUESTRA EN UNA LISTA
def agregar_ingrediente(request):
    cantidad = request.GET.get('cantidad', None)
    medida = request.GET.get('medida', None)
    ingrediente = request.GET.get('ingrediente', None)
    id_receta = int(request.GET.get('id_receta')) + 1
    #SI EL NOMBRE DEL INGREDIENTE EXISTE EN LA LISTA DE LA RECETA ENVIA UN MENSAJE VIA AJAX
    ingredientes_agregados = Ingrediente.objects.filter(id_receta=id_receta)
    for i in ingredientes_agregados:
        if i.nombre == ingrediente:
            return HttpResponse('<li id="existe">Ya existe ese ingrediente</li>')
    #CREA UN INGREDIENTE PARA LA RECETA CON SU RESPECTIVA CANTIDAD Y UNIDAD DE MEDIDA
    i = Ingrediente()
    i.id_receta = id_receta
    i.nombre = ingrediente
    i.cantidad = cantidad
    i.medida = medida
    i.save()
    #ENVIA VIA AJAX A LA PAGINA UN ITEM DE LISTA CON BOTON QUITAR INGREDIENTE
    boton_quitar = ' <button class="btn btn-danger" data-ingrediente='+ingrediente+' id="quitar" type="button">Quitar</button>'
    response = '<li>'+cantidad+' '+medida+' '+ingrediente+boton_quitar+'</li>'
    return HttpResponse(response)

#QUITA VIA AJAX UN INGREDIENTE Y LO QUITA DE LA LISTA
def quitar_ingrediente(request):
    ingrediente = request.GET.get('ingrediente', None)
    id_receta = int(request.GET.get('id_receta')) + 1
    Ingrediente.objects.filter(id_receta=id_receta).filter(nombre=ingrediente).delete()
    return HttpResponse('')

