from django.forms import ModelForm
from .models import *

class RecetaForm(ModelForm):
    class Meta:
        model=Receta
        fields=('titulo','imagen','tipo','categoria','preparacion')

class IngredienteForm(ModelForm):
    class Meta:
        model=Ingrediente
        fields=('__all__')