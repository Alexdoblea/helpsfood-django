from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from cliente.views import *

app_name='cliente'

urlpatterns = [
    path('registrar_usuario/', registrar_usuario, name='registrar_usuario'),
    path('iniciar_sesion/', iniciar_sesion, name='iniciar_sesion'),
    path('perfil/', perfil, name="perfil"),
    path('editar_datos/', editar_datos, name="editar_datos"),
    path('cambiar_contrasena/', cambiar_contrasena, name="cambiar_contrasena"),
    path('cerrar_sesion/', cerrar_sesion, name="cerrar_sesion"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)