from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from gestion_usuarios.forms import *
from .models import *
from django.contrib import messages


#IMPORTANTE: siempre todos los usuarios que se registren perteneceran al grupo "clientes"
#en cada funcion se verifica el grupo al cual pertenece el usuario

# ESTA ES LA VISTA PRINCIPAL DE LA PAGINA, SI EL CLIENTE ESTÁ LOGEADO LO REDIRIGIRA A SU PERFIL
def inicio(request):
    if not request.user.groups.filter(name__in=['clientes']).exists():
        return render(request, 'inicio.html')
    else:
        return redirect('cliente:perfil')

# REGISTRA UN USUARIO MEDIANTE EL METODO POST
def registrar_usuario(request):
    if not request.user.groups.filter(name__in=['clientes']).exists():
        form = Registrarse
        if request.method=='POST':
            form = Registrarse(data=request.POST)
            if form.is_valid():
                user=form.save()
                group=Group.objects.get_or_create(name='clientes')
                group=Group.objects.get(name='clientes')
                user.groups.add(group)
                messages.add_message(request, messages.SUCCESS, "Cliente Registrado Exitosamente. Inicie Sesión", extra_tags='cliente_registrado')
                return redirect('cliente:iniciar_sesion')
            else:
                errores = form.errors
                form = Registrarse
                return render(request, 'cliente/registrarse.html',{'form':form, 'errores':errores})
        return render(request, 'cliente/registrarse.html',{'form':form})
    else:
        if request.user.groups.filter(name__in=['clientes']).exists():
            return redirect('cliente:perfil')

#PERMITE EL INICIO DE SESION DEL CLIENTE Y LO REDIRIGE A SU PERFIL
def iniciar_sesion(request):
    form = Iniciar_sesionform
    if request.method=='POST':
        form = Iniciar_sesionform(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user=authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if request.user.groups.filter(name__in=['clientes']).exists():
                    formpass=CambiarPassForm(request.user)
                    contexto={'formpass':formpass}
                    return render(request, 'cliente/perfil.html', contexto)
                else:
                    logout(request)
                    contexto={'form':form}
                    return render(request, 'cliente/iniciar_sesion.html', contexto)
            else:
                logout(request)
                contexto={'form':form}
                return render(request, 'cliente/iniciar_sesion.html', contexto)
    return render(request, 'cliente/iniciar_sesion.html', {'form':form})

# DIRIGE AL PERFIL DEL CLIENTE, REQUIERE ESTAR LOGEADO PARA PODER ACCEDER, DE LO CONTRARIO SE REDIRIGE A INICIAR SESION
@login_required(login_url='cliente:iniciar_sesion')
def perfil(request):
    if request.user.is_authenticated:
        formpass=CambiarPassForm(request.user)
        if request.user.groups.filter(name__in=['clientes']).exists():
            contexto={'formpass':formpass}
            return render(request, 'cliente/perfil.html', contexto)
        else:
            return redirect('cliente:cerrar_sesion')
    else:
        return redirect('cliente:cerrar_sesion')

# PERMITE EDITAR LOS DATOS DE UN CLIENTE DESDE SU PERFIL, REQUIERE ESTAR LOGEADO
@login_required(login_url='cliente:iniciar_sesion')
def editar_datos(request):
    if request.user.groups.filter(name__in=['clientes']).exists():
        formpass=CambiarPassForm(request.user)
        contexto={'formpass':formpass}
        if request.method=='POST':
            cliente = Usuario.objects.filter(id=request.user.id)
            email=request.POST.get('email')
            first_name=request.POST.get('first_name')
            last_name=request.POST.get('last_name')
            cliente.update(email=email,
                        first_name=first_name,
                        last_name=last_name)
            messages.add_message(request, messages.SUCCESS, "Datos Actualizados Exitosamente", extra_tags='datos_actualizados')
            return redirect('cliente:perfil')
        return render(request, 'cliente/perfil.html', contexto)
    else:
        return redirect('cliente:cerrar_sesion')

# PERMITE CAMBIAR LA CONTRASEÑA DESDE EL PERFIL DEL CLIENTE, REQUIERE ESTAR LOGEADO
@login_required(login_url='cliente:iniciar_sesion')
def cambiar_contrasena(request):
    if request.user.groups.filter(name__in=['clientes']).exists():
        formpass = CambiarPassForm(request.user)
        contexto={'formpass':formpass}
        if request.method=='POST':
            formpass = CambiarPassForm(request.user, request.POST)
            if formpass.is_valid():
                formpass.save()
                formpass = CambiarPassForm(request.user)
                form=Iniciar_sesionform
                contexto={'form':form,
                        'passcambiado':True}
                return render(request, 'cliente/iniciar_sesion.html', contexto)
            else:
                errores_pass=formpass.errors
                formpass = CambiarPassForm(request.user)
                contexto={'formpass':formpass,
                        'errores_pass':errores_pass,
                        'passnocambiado':True
                        }
                return render(request, 'cliente/perfil.html', contexto)
        return redirect('cliente:iniciar_sesion')
    else:
        return redirect('cliente:cerrar_sesion')

# PERMITE TERMINAR O CERRAR LA SESION ACTIVA
@login_required(login_url='cliente:iniciar_sesion')
def cerrar_sesion(request):
    logout(request)
    return redirect('cliente:iniciar_sesion')


