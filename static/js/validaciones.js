$(function() {
        jQuery.validator.addMethod("soloLETRAS", function soloLETRAS(palabra) {
                var regexp = /^[a-zA-Z ]+$/
                return regexp.test(palabra);
        }, 'Solo debe contener letras');

        $("#inicia_sesion_cliente").validate({
                rules: {
                        username:{required:true},
                        password:{required:true}
                        
                },
                messages: {
                        username:{required:'Debe ingresar su nombre de usuario'},
                        password:{required:'Debe ingresar su contraseña'}
                }
        });

        $("#cambiar_contrasena_cliente").validate({
                rules: {
                        old_password:{required:true},
                        new_password1:{required:true},
                        new_password2:{required:true}
                        
                },
                messages: {
                        old_password:{required:'Debe ingresar su contraseña actual'},
                        new_password1:{required:'Debe ingresar su nueva contraseña'},
                        new_password2:{required:'Debe confirmar su nueva contraseña'}
                }
        });


        $("#registrarse").validate({
        rules: {
                username:{required:true, minlength:5, maxlength:25},
                email:{required:true, minlength:5, maxlength:50, email:true},
                first_name:{required:true, minlength:3, maxlength:25, soloLETRAS:true},
                last_name:{required:true, minlength:3, maxlength:25, soloLETRAS:true},
                password1:{required:true},
                password2:{required:true},
        },
        messages: {
                username:{required:'Campo Obligatorio',
                        minlength:'El nombre de usuario debe poseer almenos 5 carácteres', 
                        maxlength:'El nombre de usuario supera el máximo de carácteres'},
                email:{required:'Campo Obligatorio',
                        email:'No posee el formato correcto: ejemplo@ejemplo.com',
                        minlength:'El email debe poseer almenos 5 caracteres', 
                        maxlength:'El email supera el máximo de carácteres'},
                first_name:{required:'Campo Obligatorio',
                        minlength:'El nombre debe al menos 3 carácteres', 
                        maxlength:'El nombre supera el máximo de carácteres'},
                last_name:{required:'Campo Obligatorio',
                        minlength:'El apellido debe poseer al menos 3 carácteres', 
                        maxlength:'El apellido supera el máximo de carácteres'},
                password1:{required:'Campo Obligatorio'},
                password2:{required:'Campo Obligatorio'}
        }
        });

        $("#actualizardatos").validate({
                rules: {
                email:{required:true, minlength:5, maxlength:50, email:true},
                first_name:{required:true, minlength:3, maxlength:25, soloLETRAS:true},
                last_name:{required:true, minlength:3, maxlength:25, soloLETRAS:true},
                },
                messages: {
                email:{required:'Campo Obligatorio',
                        email:'No posee el formato correcto: ejemplo@ejemplo.com',
                        minlength:'El email debe poseer almenos 5 caracteres', 
                        maxlength:'El email supera el máximo de carácteres'},
                first_name:{required:'Campo Obligatorio',
                        minlength:'El nombre debe al menos 3 carácteres', 
                        maxlength:'El nombre supera el máximo de carácteres'},
                last_name:{required:'Campo Obligatorio',
                        minlength:'El apellido debe poseer al menos 3 carácteres', 
                        maxlength:'El apellido supera el máximo de carácteres'},
                }
                });
        
});