from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import Usuario
from django.contrib.auth.forms import PasswordChangeForm


class Registrarse(UserCreationForm):
    class Meta:
        model=Usuario
        fields=('username',
                'email',
                'first_name',
                'last_name',
                'password1',
                'password2')

    def __init__(self, *args, **kwargs):
        super(Registrarse, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
    
class Iniciar_sesionform(AuthenticationForm):
    class Meta:
        model=Usuario
        fields=('username','password')

    def __init__(self, *args, **kwargs):
        super(Iniciar_sesionform, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class CambiarPassForm(PasswordChangeForm):

    def __init__(self, *args, **kwargs):
        super(CambiarPassForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'